package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by odedf on 13/10/2016.
 */
public class LoggerWrapper {

    private Logger logger = null;

    public LoggerWrapper(String name) {
        this.logger = LoggerFactory.getLogger(name);
    }

    public void debug(String message)
    {
        logger.debug(message);
    }

    public void debugFormat(String methodName, String format, Object... args)
    {
        logger.debug(String.format(methodName + "() " + format, args));
    }

    public void error(String message)
    {
        logger.error(message);
    }

    public void errorFormat(String methodName, String format, Object... args)
    {
        logger.error(String.format(methodName + "() " + format, args));
    }

    public void info(String message)
    {
        logger.info(message);
    }

    public void infoFormat(String methodName, String format, Object... args)
    {
        logger.info(String.format(methodName + "() " + format, args));
    }

    public void warn(String message)
    {
        logger.warn(message);
    }

    public void warnFormat(String methodName, String format, Object... args)
    {
        logger.warn(String.format(methodName + "() " + format, args));
    }

}

