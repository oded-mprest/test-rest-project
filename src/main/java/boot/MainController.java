package boot;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import utils.LoggerWrapper;
import utils.SpringLoggingHelper;

/**
 * Created by odedf on 10/10/2016.
 */
@CrossOrigin
@RestController
public class MainController {


    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/ping")
    public String ping(@RequestParam(value="name", defaultValue="World") String name) {

        try {

            logger.debug("This is a debug message");
            logger.info("This is an info message");

            logger.warn("This is a warn message");
            logger.error("This is an error message");
        } catch (Exception e) {
            e.printStackTrace();
        }

        new SpringLoggingHelper().helpMethod();

        return "pong";
    }
}
